import marked from "marked"
import { fetchText } from "./fetch.js"

export function getSourceRenderType(source) {
	return "content"
}

export async function fetchCardsPageData(source, config, context) {
	throw new Error("Not implemented: markdown fetcher only returns markdown url, not cards")
}

export async function fetchContentPageData(source, config, context) {
	const markdown = await fetchText(source.file, context)
	const html = marked(markdown)
	return {
		html,
	}
}
